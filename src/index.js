import express from 'express';

const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', function(req,res) {
	var a = req.query.a;
	var b = req.query.b
	if (isNaN(a)) a = 0;
	if (isNaN(b)) b = 0;
	var c = +a + +b;
	return res.send(`${c}`, {'Content-Type': 'text/plain'}, 200);

});

app.listen(3000, function() {
});


